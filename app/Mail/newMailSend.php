<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use stdClass;

class newMailSend extends Mailable
{
    use Queueable, SerializesModels;
    private $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(stdClass $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //para testes, sem enviar o email de fato
        /*  return $this->view('mail.newMailSend', [
            'user' => $this->user
        ])
            ->from($this->user->email, $this->user->name)
            ->subject('Confirmação de Cadastro - FINX'); */

        $this->subject('Confirmação de Cadastro - FINX');
        $this->to($this->user->email, $this->user->name);
        return $this->markdown('mail.newMailSend', [
            'user' => $this->user
        ]);
    }
}
