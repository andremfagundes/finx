<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Rota pública do Auth
Route::group(['prefix' => 'auth'], function ($router) {
    Route::post('login', 'Api\\AuthController@login');
    Route::post('register', 'Api\\AuthController@register');
    Route::post('refresh-token', 'Api\\AuthController@refresh');
});

//rotas protegidas do Auth
Route::group(['middleware' => 'apiJwt', 'prefix' => 'auth'], function ($router) {
    Route::post('logout', 'Api\\AuthController@logout');
    Route::get('me', 'Api\\AuthController@me');
});

//rotas protegidas do Prevision
Route::group(['middleware' => 'apiJwt', 'prefix' => 'prevision'], function ($router) {
    Route::post('releases/{filter}', 'Api\\PrevisionController@fetchReleasesItems');
});
