@component('mail::message')
<h1>
    Confirmação de Cadastro - FINX
</h1>
<p>
    Olá {{ $user->name }}, você está quase lá! Clique no botão abaixo e confirme seu cadastro para poder acessar o Finx!
</p>
<p>
    @component('mail::button',['url'=>'https://www.globo.com'])
    Confirmar Cadastro
    @endcomponent
</p>
<p>
    Obrigado,<br />
    Finx - Finanças Pessoais
</p>

@endcomponent
