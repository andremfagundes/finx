import Vue from 'vue'
import Router from 'vue-router'
import auth from "@/auth/authService";

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior() {
        return { x: 0, y: 0 }
    },
    routes: [

        {
            // =============================================================================
            // MAIN LAYOUT ROUTES
            // =============================================================================
            path: '',
            component: () =>
                import ('./layouts/main/Main.vue'),
            children: [
                // =============================================================================
                // Theme Routes
                // =============================================================================
                {
                    path: '/',
                    name: 'home',
                    component: () =>
                        import ('./views/Home.vue'),
                    meta: {
                        rule: 'editor',
                        authRequired: true
                    }
                },
                {
                    path: '/previsao',
                    name: 'prevision',
                    component: () =>
                        import ('./views/prevision/Prevision.vue'),
                    meta: {
                        rule: 'editor',
                        authRequired: true
                    }
                },
            ],
        },
        // =============================================================================
        // FULL PAGE LAYOUTS
        // =============================================================================
        {
            path: '',
            component: () =>
                import ('@/layouts/full-page/FullPage.vue'),
            children: [
                // =============================================================================
                // PAGES
                // =============================================================================
                {
                    path: '/pages/login',
                    name: 'page-login',
                    component: () =>
                        import ('@/views/pages/login/Login.vue'),
                    meta: {
                        rule: 'public',
                        authRequired: false
                    }
                },
                {
                    path: '/pages/register',
                    name: 'page-register',
                    component: () =>
                        import ('@/views/pages/register/Register.vue'),
                    meta: {
                        rule: 'public',
                        authRequired: false
                    }
                },
                {
                    path: '/pages/forgot-password',
                    name: 'page-forgot-password',
                    component: () =>
                        import ('@/views/pages/ForgotPassword.vue'),
                    meta: {
                        rule: 'public',
                        authRequired: false
                    }
                },
                {
                    path: '/pages/reset-password',
                    name: 'page-reset-password',
                    component: () =>
                        import ('@/views/pages/ResetPassword.vue'),
                    meta: {
                        rule: 'public',
                        authRequired: false
                    }
                },
                {
                    path: '/pages/error-404',
                    name: 'page-error-404',
                    component: () =>
                        import ('@/views/pages/Error404.vue'),
                    meta: {
                        rule: 'public'
                    }
                },
                {
                    path: '/pages/error-500',
                    name: 'page-error-500',
                    component: () =>
                        import ('@/views/pages/Error500.vue'),
                    meta: {
                        rule: 'public'
                    }
                },
                {
                    path: '/pages/not-authorized',
                    name: 'page-not-authorized',
                    component: () =>
                        import ('@/views/pages/NotAuthorized.vue'),
                    meta: {
                        rule: 'public'
                    }
                },
            ]
        },
        // Redirect to 404 page, if no match found
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})

router.afterEach(() => {
    // Remove initial loading
    const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = "none";
    }
});

router.beforeEach((to, from, next) => {
    // If auth required, check login. If login fails redirect to login page
    if (to.meta.authRequired && !(auth.isAuthenticated())) {
        //window.location.href = "/pages/login"
        next({ name: 'page-login' });
    } else {
        next();
    }

    // Specify the current path as the customState parameter, meaning it
    // will be returned to the application after auth
    // auth.login({ target: to.path });

});

export default router