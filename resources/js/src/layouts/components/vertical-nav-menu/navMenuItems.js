export default [{
        url: "/",
        name: "Home",
        slug: "home",
        icon: "HomeIcon",
    },
    {
        url: "/previsao",
        name: "Previsão",
        slug: "previsao",
        icon: "BarChart2Icon",
    },
]