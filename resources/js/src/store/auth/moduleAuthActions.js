import jwt from "../../http/requests/auth/jwt/index.js"

import router from '@/router'

export default {

    updateUsername({ commit }, payload) {
        payload.user.updateProfile({
            name: payload.name
        }).then(() => {

            let newUserData = Object.assign({}, payload.user.providerData[0])
            newUserData.name = payload.name
            commit('UPDATE_USER_INFO', newUserData, { root: true })

            if (payload.isReloadRequired) {
                router.push(router.currentRoute.query.to || '/')
            }
        }).catch((err) => {
            payload.notify({
                time: 8800,
                title: 'Error',
                text: err.message,
                iconPack: 'feather',
                icon: 'icon-alert-circle',
                color: 'danger'
            })
        })
    },

    // JWT
    loginJWT({ commit }, payload) {

        return new Promise((resolve, reject) => {
            jwt.login(payload.userDetails.email, payload.userDetails.password)
                .then(response => {
                    if (response.data.userData) {

                        //adiciona no localstorage 'tokenExpiry' e 'loggedIn'
                        localStorage.setItem("tokenExpiry", response.data.expiresIn)
                        localStorage.setItem("loggedIn", true)

                        //adiciona dados do usuario no localstorage 'userInfo'
                        commit('UPDATE_USER_INFO', response.data.userData.original, { root: true })

                        //adiciona no cabeçalho o token
                        commit("SET_BEARER", response.data.accessToken)

                        resolve(response)
                    } else {
                        reject({ message: "Email ou Senha inválidos!" })
                    }
                })
                .catch(error => { reject(error) })
        })
    },
    // JWT Logout
    logoutJWT() {
        return new Promise((resolve, reject) => {
            jwt.logout()
                .then(response => {
                    if (response.data) {
                        // If JWT login
                        if (localStorage.getItem("tokenExpiry")) {
                            localStorage.removeItem("tokenExpiry");
                        }
                        if (localStorage.getItem("userInfo")) {
                            localStorage.removeItem("userInfo");
                        }
                        localStorage.setItem("loggedIn", false);
                        resolve(response)
                    } else {
                        reject({ message: "Houve algum erro ao sair!" })
                    }

                })
                .catch(error => { reject(error) })
        })
    },

    registerUserJWT({ commit }, payload) {

        const { name, email, password, confirmPassword } = payload.userDetails

        return new Promise((resolve, reject) => {

            // Check confirm password
            if (password !== confirmPassword) {
                reject({ message: "A senha não corresponde. Por favor, tente novamente." });
            }

            jwt.registerUser(name, email, password)
                .then(response => {
                    resolve(response)
                })
                .catch(error => { reject(error) })
        })
    },
    fetchAccessToken() {
        return new Promise((resolve) => {
            jwt.refreshToken().then(response => { resolve(response) })
        })
    }
}
