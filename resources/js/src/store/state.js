import navbarSearchAndPinList from "@/layouts/components/navbar/navbarSearchAndPinList"
import themeConfig from "@/../themeConfig.js"
import colors from "@/../themeConfig.js"

let userDefaults = {}
    // /////////////////////////////////////////////
    // Variables
    // /////////////////////////////////////////////
if (localStorage.getItem("userInfo")) {
    let user = JSON.parse(localStorage.getItem("userInfo"));

    userDefaults = {
        uid: user.uid, // From Auth
        displayName: user.displayName, // From Auth
        about: user.about,
        photoURL: user.photoURL, // From Auth
        status: "online",
        userRole: user.userRole
    }
} else {

    userDefaults = {
        uid: 0, // From Auth
        displayName: "André", // From Auth
        about: "Empreendedor, Desenvolvedor Web, Filho de DEUS, Fundador de várias empresas na Web!",
        photoURL: require("@assets/images/portrait/small/avatar-s-11.jpg"), // From Auth
        status: "online",
        userRole: "admin"
    }
}

// /////////////////////////////////////////////
// State
// /////////////////////////////////////////////

const state = {
    AppActiveUser: userDefaults,
    bodyOverlay: false,
    isVerticalNavMenuActive: true,
    mainLayoutType: themeConfig.mainLayoutType || "vertical",
    navbarType: themeConfig.navbarType || "floating",
    navbarSearchAndPinList: navbarSearchAndPinList,
    reduceButton: themeConfig.sidebarCollapsed,
    verticalNavMenuWidth: "default",
    verticalNavMenuItemsMin: false,
    scrollY: 0,
    starredPages: navbarSearchAndPinList["pages"].data.filter((page) => page.is_bookmarked),
    theme: themeConfig.theme || "light",
    themePrimaryColor: colors.primary,

    // Can be used to get current window with
    // Note: Above breakpoint state is for internal use of sidebar & navbar component
    windowWidth: null,

}

export default state
