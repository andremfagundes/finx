/*=========================================================================================
  File Name: modulePrevisionGetters.js
  Description: Prevision Module Getters
  ----------------------------------------------------------------------------------------
  Item Name: Finx
  Author: André M Fagundes
  Author URL: https://github.com/andrefagundes
==========================================================================================*/


export default {
    getTotalReceiveds: state => itemId => {
        const index = state.previsions.findIndex((item) => item.id == itemId)
        return state.previsions[index].releases
            .map(prod =>
                prod.receiveds.reduce((total, valor) => total + valor.received, 0)
            )
            .reduce((total, valor) => total + valor, 0)
    },
    getTotalPlanned: state => itemId => {
        const index = state.previsions.findIndex((item) => item.id == itemId)
        return state.previsions[index].releases.reduce((total, valor) => total + valor.planned, 0)
    },
    getTotalValueReceivedsLocal: state =>
        state.receiveds_local.reduce(
            (total, valor) => total + valor.received,
            0
        ),
    getTotalDataReceivedsLocal: state =>
        state.receiveds_local.length
}