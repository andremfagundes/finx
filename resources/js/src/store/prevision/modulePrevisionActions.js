/*=========================================================================================
  File Name: modulePrevisionActions.js
  Description: Prevision Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Finx
  Author: André M Fagundes
  Author URL: https://github.com/andrefagundes
==========================================================================================*/

import axios from "@/axios.js"

export default {
    addItem({ commit }, item) {
        return new Promise((resolve, reject) => {
            axios.post("/api/prevision/releases/", { item: item })
                .then((response) => {
                    commit('ADD_ITEM', Object.assign(item, { id: response.data.id }))
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    fetchPrevisions({ commit }, payload) {
        return new Promise((resolve, reject) => {
            axios.get(`/api/previsions/`)
                .then((response) => {
                    commit('SET_PREVISIONS', response.data)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    updateItem({ commit }, item) {
        return new Promise((resolve, reject) => {
            axios.post(`/api/prevision/releases/${item.id}`, { item: item })
                .then((response) => {
                    commit('UPDATE_RELEASES', response.data)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    removeItem({ commit }, itemId) {
        return new Promise((resolve, reject) => {
            axios.delete(`/api/prevision/releases/${itemId}`)
                .then((response) => {
                    commit('REMOVE_ITEM', itemId)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    addReceivedLocal({ commit }, received) {
        commit('ADD_RECEIVED_LOCAL', Object.assign({}, received));
    },
    removeReceivedLocal({ commit }, itemId) {
        commit('REMOVE_RECEIVED_LOCAL', itemId);
    }
}
