/*=========================================================================================
  File Name: modulePrevisionMutations.js
  Description: Prevision Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Finx
  Author: André M Fagundes
  Author URL: https://github.com/andrefagundes
==========================================================================================*/


export default {
    SET_PREVISIONS(state, previsions) {
        state.previsions = previsions;
    },
    ADD_ITEM(state, item) {
        state.releases.unshift(item)
    },
    SET_RELEASES(state, releases) {
        state.releases = releases;
    },
    UPDATE_RELEASES(state, release) {
        const releaseIndex = state.releases.findIndex((p) => p.id == release.id)
        Object.assign(state.releases[releaseIndex], release)
    },
    REMOVE_ITEM(state, itemId) {
        const ItemIndex = state.releases.findIndex((p) => p.id == itemId)
        state.releases.splice(ItemIndex, 1)
    },

    //RECEIVEDS LOCAL
    SET_RECEIVEDS_LOCAL(state, receiveds) {
        state.receiveds_local = receiveds;
    },
    ADD_RECEIVED_LOCAL(state, receivedLocal) {
        state.receiveds_local.unshift(receivedLocal);
    },
    REMOVE_RECEIVED_LOCAL(state, itemId) {
        const ItemIndex = state.receiveds_local.findIndex((p) => p.id == itemId)
        state.receiveds_local.splice(ItemIndex, 1)
    },
    INIT_RECEIVEDS_LOCAL(state) {
        state.receiveds_local = [];
    }
}