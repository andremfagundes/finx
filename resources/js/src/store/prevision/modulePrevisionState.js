/*=========================================================================================
  File Name: modulePrevisonState.js
  Description: Prevision Module State
  ----------------------------------------------------------------------------------------
  Item Name: Finx
  Author: André M Fagundes
  Author URL: https://github.com/andrefagundes
==========================================================================================*/

export default {
    previsions: [],
    receiveds_local: []
}