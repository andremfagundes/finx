/*=========================================================================================
  File Name: modulePrevision.js
  Description: Prevision Module
  ----------------------------------------------------------------------------------------
  Item Name: Finx
  Author: André M Fagundes
  Author URL: https://github.com/andrefagundes
==========================================================================================*/


import state from './modulePrevisionState.js'
import mutations from './modulePrevisionMutations.js'
import actions from './modulePrevisionActions.js'
import getters from './modulePrevisionGetters.js'

export default {
    isRegistered: false,
    namespaced: true,
    state: state,
    mutations: mutations,
    actions: actions,
    getters: getters
}