import mock from "@/fake-db/mock.js"

const data = {
    previsions: [{
        'id': 1,
        'userId': 1,
        "title": 'Renda',
        'description': "Minhas redas hoje",
        releases: [{
                "id": 1,
                "previsionId": 1,
                "name": "Clínica Cotta",
                "planned": 4324.00,
                "date": 1591106034000,
                "receiveds": [{ releaseId: 1, name: 'received 1', received: 434.45, date: 1591106034000 }]
            },
            {
                "id": 2,
                "previsionId": 1,
                "name": "Freelancer",
                "planned": 3543.99,
                "date": 1591106034000,
                "receiveds": [
                    { releaseId: 2, name: 'received 2', received: 35.45, date: 1591106034000 },
                    { releaseId: 2, name: 'received 2', received: 35.10, date: 1591106034000 }
                ]
            },
            {
                "id": 3,
                "previsionId": 1,
                "name": "Altec Lansing - Bluetooth Speaker",
                "planned": 3235.00,
                "date": 1591106034000,
                "receiveds": [{ releaseId: 3, name: 'received 3', received: 57.33, date: 1591106034000 }]
            },
        ]
    }, {
        'id': 2,
        'userId': 1,
        "title": 'Alimentação',
        'description': "",
        releases: [{
                "id": 1,
                "previsionId": 2,
                "name": "Supermercado",
                "planned": 4324.00,
                "date": 1591106034000,
                "receiveds": [{ name: 'Supermercado Eta', received: 434.45, date: 1591106034000 }]
            },
            {
                "id": 2,
                "previsionId": 2,
                "name": "Lanches",
                "planned": 3543.99,
                "date": 1591106034000,
                "receiveds": [
                    { name: 'girafas', received: 35.45, date: 1591106034000 },
                    { name: 'restaurante', received: 35.10, date: 1591106034000 }
                ]
            },
        ]
    }]
}


mock.onGet("/api/previsions/").reply(() => {
    return [200, JSON.parse(JSON.stringify(data.previsions)).reverse()];
});

// POST : Add new prevision
mock.onPost("/api/prevision/releases/").reply((request) => {
    console.log(request);
    console.log(data);
    // Get event from post data
    let item = JSON.parse(request.data).item

    const length = data.releases.length
    let lastIndex = 0
    if (length) {
        lastIndex = data.releases[length - 1].id
    }
    item.id = lastIndex + 1

    data.releases.push(item)

    return [201, { id: item.id }]
})

// Update Release
mock.onPost(/\/api\/prevision\/releases\/\d+/).reply((request) => {

    const itemId = request.url.substring(request.url.lastIndexOf("/") + 1)

    let item = data.releases.find((item) => item.id == itemId)
    Object.assign(item, JSON.parse(request.data).item)

    return [200, item]
})

// DELETE: Remove Item
mock.onDelete(/\/api\/prevision\/releases\/\d+/).reply((request) => {

    const itemId = request.url.substring(request.url.lastIndexOf("/") + 1)

    const itemIndex = data.releases.findIndex((p) => p.id == itemId)
    data.releases.splice(itemIndex, 1)
    return [200]
})
