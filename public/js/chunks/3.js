(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/Prevision.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/Prevision.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var vuejs_datepicker_src_locale__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuejs-datepicker/src/locale */ "./node_modules/vuejs-datepicker/src/locale/index.js");
/* harmony import */ var _PrevisionAddNewGroup_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./PrevisionAddNewGroup.vue */ "./resources/js/src/views/prevision/PrevisionAddNewGroup.vue");
/* harmony import */ var _store_prevision_modulePrevision_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/store/prevision/modulePrevision.js */ "./resources/js/src/store/prevision/modulePrevision.js");
/* harmony import */ var _PrevisionCard_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./PrevisionCard.vue */ "./resources/js/src/views/prevision/PrevisionCard.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__["default"],
    NewGroup: _PrevisionAddNewGroup_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    PrevisionCard: _PrevisionCard_vue__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  data: function data() {
    return {
      dateMonthLocal: new Date(Date.now()),
      language: "ptBR",
      languages: vuejs_datepicker_src_locale__WEBPACK_IMPORTED_MODULE_1__,
      format: "MMMM yyyy",
      money: {
        decimal: ",",
        thousands: ".",
        prefix: "R$",
        precision: 2,
        masked: false
        /* doesn't work with directive */

      }
    };
  },
  created: function created() {
    this.toogleNavBar();
    this.$store.registerModule("prevision", _store_prevision_modulePrevision_js__WEBPACK_IMPORTED_MODULE_3__["default"]);
    _store_prevision_modulePrevision_js__WEBPACK_IMPORTED_MODULE_3__["default"].isRegistered = true;
    this.initiateMonth();
  },
  computed: {
    previsions: {
      get: function get() {
        return this.$store.state.prevision.previsions;
      },
      set: function set(value) {
        var data = Object.assign([], value);
        this.$store.commit("prevision/SET_PREVISIONS", data);
      }
    }
  },
  watch: {
    "$store.state.windowWidth": function $storeStateWindowWidth() {
      this.toogleNavBar();
    }
  },
  methods: {
    initiateMonth: function initiateMonth() {
      this.$store.dispatch("prevision/fetchPrevisions", this.$options.filters.monthYear(this.dateMonthLocal));
    },
    cloneMesAnterior: function cloneMesAnterior() {
      alert("CLONAR");
    },
    toogleNavBar: function toogleNavBar() {
      this.$store.state.windowWidth > 1200 ? this.$store.commit("UPDATE_NAV_BAR_TYPE", "hidden") : this.$store.commit("UPDATE_NAV_BAR_TYPE", "floating");
    },
    openPicker: function openPicker() {
      this.$refs.programaticOpen.showCalendar();
    }
  },
  filters: {
    monthYear: function monthYear(data) {
      var month = new Date(data);
      return month.getMonth() + 1 + "/" + month.getFullYear();
    }
  },
  beforeDestroy: function beforeDestroy() {
    this.$store.unregisterModule("prevision");
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      activePrompt: false,
      previsionLocal: {
        title: "",
        desc: ""
      }
    };
  },
  computed: {
    validateForm: function validateForm() {
      return !this.errors.any() && this.previsionLocal.title != "";
    }
  },
  methods: {
    clearFields: function clearFields() {
      Object.assign(this.previsionLocal, {
        title: "",
        desc: ""
      });
    },
    addGroup: function addGroup() {
      var _this = this;

      this.$validator.validateAll().then(function (result) {
        if (result) {
          _this.$store.dispatch("prevision/addItem", Object.assign({}, _this.previsionLocal));

          _this.clearFields();
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ui_elements_prevision_DataViewSidebar_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ui-elements/prevision/DataViewSidebar.vue */ "./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue");
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuedraggable */ "./node_modules/vuedraggable/dist/vuedraggable.common.js");
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vuedraggable__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "prevision-card",
  components: {
    DataViewSidebar: _ui_elements_prevision_DataViewSidebar_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    draggable: vuedraggable__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  data: function data() {
    return {
      addNewDataSidebar: false,
      sidebarData: {}
    };
  },
  props: ["keyPrevision", "id", "title", "description"],
  computed: {
    releases: {
      get: function get() {
        return this.$store.state.prevision.previsions[this.keyPrevision].releases;
      },
      set: function set(value) {
        console.log("key:" + this.keyPrevision);
        console.log(value);
        var data = Object.assign([], value);
        /*  this.$store.commit("prevision/SET_PREVISIONS", data);
        this.$store.commit("setPagesGroup", {
          value,
          key: this.id
        }); */
      }
    }
  },
  methods: {
    addNewData: function addNewData() {
      this.sidebarData = {};
      this.toggleDataSidebar(true);
    },
    editData: function editData(data) {
      this.sidebarData = data;
      this.toggleDataSidebar(true);
    },
    deleteData: function deleteData(id) {
      this.$store.dispatch("prevision/removeItem", id).catch(function (err) {
        console.error(err);
      });
    },
    totalReceived: function totalReceived(receiveds) {
      var total = receiveds.reduce(function (total, valor) {
        return total + valor.received;
      }, 0);
      return total;
    },
    totalPlanned: function totalPlanned(id) {
      return this.$store.getters["prevision/getTotalPlanned"](id);
    },
    totalReceiveds: function totalReceiveds(id) {
      return this.$store.getters["prevision/getTotalReceiveds"](id);
    },
    toggleDataSidebar: function toggleDataSidebar() {
      var val = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      this.addNewDataSidebar = val;
    },
    checkMove: function checkMove(value) {
      console.log(value.relatedContext.list);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var v_money__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! v-money */ "./node_modules/v-money/dist/v-money.js");
/* harmony import */ var v_money__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(v_money__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var vuejs_datepicker_src_locale__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuejs-datepicker/src/locale */ "./node_modules/vuejs-datepicker/src/locale/index.js");
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vuedraggable */ "./node_modules/vuedraggable/dist/vuedraggable.common.js");
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vuedraggable__WEBPACK_IMPORTED_MODULE_3__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Money: v_money__WEBPACK_IMPORTED_MODULE_0__["Money"],
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_1__["default"],
    draggable: vuedraggable__WEBPACK_IMPORTED_MODULE_3___default.a
  },
  data: function data() {
    return {
      dataDesc: "",
      //calendar
      language: "ptBR",
      languages: vuejs_datepicker_src_locale__WEBPACK_IMPORTED_MODULE_2__,
      format: "dd MMMM yyyy",
      activeAddReceived: false,
      receivedLocal: {
        name: "",
        received: 0,
        date: new Date(Date.now())
      },
      money: {
        decimal: ",",
        thousands: ".",
        precision: 2,
        masked: false
      }
    };
  },
  computed: {
    validateForm: function validateForm() {
      return this.receivedLocal.name != "" && this.receivedLocal.received > 0;
    },
    totalDataReceivedsLocal: function totalDataReceivedsLocal() {
      return this.$store.getters["prevision/getTotalDataReceivedsLocal"];
    },
    totalValueReceivedsLocal: function totalValueReceivedsLocal() {
      return this.$store.getters["prevision/getTotalValueReceivedsLocal"];
    },
    receivedsListLocal: {
      get: function get() {
        return this.$store.state.prevision.receiveds_local;
      },
      set: function set(value) {
        var data = Object.assign([], value);
        this.$store.commit("prevision/SET_RECEIVEDS_LOCAL", data);
      }
    }
  },
  methods: {
    addReceivedLocal: function addReceivedLocal() {
      var _this = this;

      this.receivedLocal.date = Date.parse(this.receivedLocal.date);
      var receivedAdd = Object.assign({}, this.receivedLocal);
      this.$store.dispatch("prevision/addReceivedLocal", receivedAdd).then(function (resolve) {
        _this.toggleAddList(false);

        _this.$vs.notify({
          text: "Incluído com sucesso!",
          color: "success",
          iconPack: "feather",
          icon: "icon-check-circle"
        });
      }).catch(function (err) {
        console.error(err);
      });
    },
    moveToReceivedLocal: function moveToReceivedLocal(index) {
      var _this2 = this;

      var itemId = index - 1;
      this.$store.dispatch("prevision/removeReceivedLocal", itemId).then(function (result) {
        _this2.$vs.notify({
          text: "Excluído com sucesso!",
          color: "success",
          iconPack: "feather",
          icon: "icon-check-circle"
        });
      }).catch(function (err) {
        console.error(err);
      });
    },
    toggleAddList: function toggleAddList(value) {
      this.activeAddReceived = value;
      this.clearFields();
    },
    clearFields: function clearFields() {
      Object.assign(this.receivedLocal, {
        name: "",
        received: 0,
        date: new Date(Date.now())
      });
    },
    openPicker: function openPicker() {
      this.$refs.programaticOpen.showCalendar();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-perfect-scrollbar */ "./node_modules/vue-perfect-scrollbar/dist/index.js");
/* harmony import */ var vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var v_money__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! v-money */ "./node_modules/v-money/dist/v-money.js");
/* harmony import */ var v_money__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(v_money__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _views_prevision_ReceivedAddNew__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/views/prevision/ReceivedAddNew */ "./resources/js/src/views/prevision/ReceivedAddNew.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    VuePerfectScrollbar: vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0___default.a,
    Money: v_money__WEBPACK_IMPORTED_MODULE_1__["Money"],
    Received: _views_prevision_ReceivedAddNew__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      dataId: null,
      dataName: "",
      dataCategory: null,
      dataPlanned: 0,
      category_previsions: [{
        text: "Renda",
        value: 1
      }, {
        text: "Doação",
        value: 2
      }, {
        text: "Transporte",
        value: 3
      }, {
        text: "Habitação",
        value: 4
      }, {
        text: "Saúde",
        value: 5
      }, {
        text: "Seguro",
        value: 6
      }, {
        text: "Pessoal",
        value: 7
      }],
      money: {
        decimal: ",",
        thousands: ".",
        precision: 2,
        masked: false
      },
      settings: {
        // perfectscrollbar settings
        maxScrollbarLength: 60,
        wheelSpeed: 0.6
      }
    };
  },
  props: {
    isSidebarActive: {
      type: Boolean,
      required: true
    },
    data: {
      type: Object,
      default: function _default() {}
    }
  },
  watch: {
    isSidebarActive: function isSidebarActive(val) {
      if (!val) return;

      if (Object.entries(this.data).length === 0) {
        this.initValues();
        this.$validator.reset();
      } else {
        var _JSON$parse = JSON.parse(JSON.stringify(this.data)),
            id = _JSON$parse.id,
            name = _JSON$parse.name,
            category = _JSON$parse.category,
            planned = _JSON$parse.planned,
            receiveds = _JSON$parse.receiveds;

        this.dataId = id;
        this.dataName = name;
        this.dataCategory = category;
        this.dataPlanned = planned;
        this.$store.commit("prevision/SET_RECEIVEDS_LOCAL", receiveds);
        this.initValues();
      }
    }
  },
  computed: {
    isSidebarActiveLocal: {
      get: function get() {
        return this.isSidebarActive;
      },
      set: function set(val) {
        if (!val) {
          this.$emit("closeSidebar");
        }
      }
    },
    isFormValid: function isFormValid() {
      return !this.errors.any() && this.dataName && this.dataCategory && this.dataPlanned > 0;
    }
  },
  methods: {
    initValues: function initValues() {
      if (this.data.id) return;
      this.dataId = null;
      this.dataName = "";
      this.dataCategory = null;
      this.dataPlanned = 0;
      this.$store.commit("prevision/INIT_RECEIVEDS_LOCAL");
    },
    submitData: function submitData() {
      var _this = this;

      this.$validator.validateAll().then(function (result) {
        if (result) {
          var obj = {
            id: _this.dataId,
            name: _this.dataName,
            category: _this.dataCategory,
            planned: _this.dataPlanned,
            receiveds: _this.$store.state.prevision.receiveds_local
          };

          if (_this.dataId !== null && _this.dataId >= 0) {
            _this.$store.dispatch("prevision/updateItem", obj).then(function (resolve) {
              _this.$vs.notify({
                text: "Alterado com sucesso!",
                color: "success",
                iconPack: "feather",
                icon: "icon-check-circle",
                position: "top-right"
              });
            }).catch(function (err) {
              console.error(err);
            });
          } else {
            delete obj.id;
            obj.popularity = 0;

            _this.$store.dispatch("prevision/addItem", obj).then(function (resolve) {
              _this.$vs.notify({
                text: "Cadastrado com sucesso!",
                color: "success",
                iconPack: "feather",
                icon: "icon-check-circle",
                position: "top-right"
              });
            }).catch(function (err) {
              console.error(err);
            });
          }

          _this.$emit("closeSidebar");

          _this.initValues();
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".list-receiveds {\n  transition: all 1s;\n}\n.list-item-component {\n  transition: background-color 0.2s, transform 0.2s;\n}\n.list-item-component:hover {\n  transition: all 0.2s;\n}\n[dir] .list-item-component:hover {\n  transform: translateY(-4px);\n  box-shadow: 0px 3px 10px 0px #ccc;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".add-new-data-sidebar[data-v-54b703a0]  .vs-sidebar--background {\n  z-index: 52010;\n}\n.add-new-data-sidebar[data-v-54b703a0]  .vs-sidebar {\n  z-index: 52010;\n  width: 400px;\n  max-width: 90vw;\n}\n.scroll-area--data-list-add-new[data-v-54b703a0] {\n  height: calc(var(--vh, 1vh) * 100 - 16px - 45px - 82px);\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/Prevision.vue?vue&type=style&index=0&module=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/Prevision.vue?vue&type=style&index=0&module=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input.vs-select-no-border {\n  outline: 0 !important;\n  font-size: 20px;\n}[dir] input.vs-select-no-border {\n  box-shadow: 0 0 0 0 !important;\n  border: 1 solid black !important;\n  background-color: #f8f8f8;\n}\n[dir] input.vs-select-no-border:hover, [dir] span.vs-select-no-border:hover {\n  background-color: #cfc1c1;\n  cursor: pointer;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ReceivedAddNew.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/Prevision.vue?vue&type=style&index=0&module=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/Prevision.vue?vue&type=style&index=0&module=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--7-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Prevision.vue?vue&type=style&index=0&module=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/Prevision.vue?vue&type=style&index=0&module=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/Prevision.vue?vue&type=template&id=3be9c2cd&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/Prevision.vue?vue&type=template&id=3be9c2cd& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "previsions" } },
    [
      _c("div", { staticClass: "vx-row mb-0 ml-1" }, [
        _c(
          "small",
          { staticClass: "flex" },
          [
            _c("datepicker", {
              ref: "programaticOpen",
              attrs: {
                format: _vm.format,
                language: _vm.languages[_vm.language],
                minimumView: "month",
                maximumView: "month",
                "input-class": "ml-auto vs-select-no-border"
              },
              model: {
                value: _vm.dateMonthLocal,
                callback: function($$v) {
                  _vm.dateMonthLocal = $$v
                },
                expression: "dateMonthLocal"
              }
            }),
            _vm._v(" "),
            _c("feather-icon", {
              staticClass: "ml-1",
              attrs: { icon: "ChevronDownIcon", svgClasses: "h-4 w-4" },
              on: { click: _vm.openPicker }
            })
          ],
          1
        )
      ]),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c("vs-divider", { staticClass: "md:w-3/5 mt-1" }),
      _vm._v(" "),
      _c("div", { staticClass: "vx-col w-full md:w-3/5 mb-base" }, [
        _vm.previsions
          ? _c(
              "div",
              [
                _vm._l(_vm.previsions, function(prevision, index) {
                  return _c(
                    "div",
                    { key: index },
                    [
                      _c("prevision-card", {
                        attrs: {
                          id: prevision.id,
                          keyPrevision: index,
                          title: prevision.title,
                          description: prevision.description
                        }
                      })
                    ],
                    1
                  )
                }),
                _vm._v(" "),
                _c("new-group")
              ],
              2
            )
          : _c("div", [
              _vm._m(1),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "vx-row items-center justify-center" },
                [
                  _c(
                    "vs-button",
                    {
                      staticClass: "items-center",
                      attrs: { color: "primary", type: "border" },
                      on: {
                        click: function($event) {
                          $event.preventDefault()
                          return _vm.cloneMesAnterior()
                        }
                      }
                    },
                    [_vm._v("Começar a Planejar")]
                  )
                ],
                1
              )
            ])
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mt-2 mb-2 ml-1" }, [
      _c("span", { staticClass: "text-success" }, [
        _vm._v("R$180,00 restantes para orçamento")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "vx-row mt-16 p-8" }, [
      _c(
        "div",
        {
          staticClass:
            "title centex items-center text-black text-info text-bold text-2xl"
        },
        [_vm._v("Parece que você ainda não possui um Orçamento para este mês.")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=template&id=432738ba&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=template&id=432738ba& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        {
          staticClass:
            "btn-add-new p-3 my-3 mt-0 w-full rounded-lg cursor-pointer flex items-center justify-center text-lg font-medium text-base text-primary border border-dashed border-primary",
          on: {
            click: function($event) {
              _vm.activePrompt = true
            }
          }
        },
        [
          _c("feather-icon", {
            attrs: { icon: "PlusIcon", svgClasses: "h-4 w-4" }
          }),
          _vm._v(" "),
          _c("span", { staticClass: "ml-2 text-base text-primary" }, [
            _vm._v("Add Grupo")
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "vs-prompt",
        {
          attrs: {
            title: "Add Grupo",
            "accept-text": "Add Grupo",
            "button-cancel": "border",
            "is-valid": _vm.validateForm,
            active: _vm.activePrompt
          },
          on: {
            cancel: _vm.clearFields,
            accept: _vm.addGroup,
            close: _vm.clearFields,
            "update:active": function($event) {
              _vm.activePrompt = $event
            }
          }
        },
        [
          _c("div", [
            _c("form", [
              _c("div", { staticClass: "vx-row" }, [
                _c(
                  "div",
                  { staticClass: "vx-col w-full" },
                  [
                    _c("vs-input", {
                      directives: [
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "w-full mb-4 mt-5",
                      attrs: {
                        name: "title",
                        placeholder: "Nome do Grupo",
                        color: _vm.validateForm ? "success" : "danger"
                      },
                      model: {
                        value: _vm.previsionLocal.title,
                        callback: function($$v) {
                          _vm.$set(_vm.previsionLocal, "title", $$v)
                        },
                        expression: "previsionLocal.title"
                      }
                    }),
                    _vm._v(" "),
                    _c("vs-textarea", {
                      attrs: { rows: "5", label: "Descrição do Grupo" },
                      model: {
                        value: _vm.previsionLocal.desc,
                        callback: function($$v) {
                          _vm.$set(_vm.previsionLocal, "desc", $$v)
                        },
                        expression: "previsionLocal.desc"
                      }
                    })
                  ],
                  1
                )
              ])
            ])
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=template&id=0201777d&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=template&id=0201777d& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "prevision-card" } },
    [
      _c("data-view-sidebar", {
        attrs: {
          isSidebarActive: _vm.addNewDataSidebar,
          data: _vm.sidebarData
        },
        on: { closeSidebar: _vm.toggleDataSidebar }
      }),
      _vm._v(" "),
      _c(
        "vx-card",
        {
          staticClass: "mb-base",
          attrs: {
            title: _vm.title,
            "title-color": "success",
            subtitle: _vm.description,
            "collapse-action": ""
          }
        },
        [
          [
            _c("div", { staticClass: "vx-row flex justify-between" }, [
              _c(
                "div",
                { staticClass: "vx-col vs-lg-5 vs-sm-4 vs-xs-4 text-2xl" },
                [_vm._v(_vm._s(_vm.title))]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "vx-col vs-lg-3 vs-sm-4 vs-xs-4 text-2xl text-right"
                },
                [_vm._v("Planejado")]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "vx-col vs-lg-3 vs-sm-4 vs-xs-4 text-2xl text-right mb-3"
                },
                [_vm._v("Recebido")]
              )
            ]),
            _vm._v(" "),
            _c(
              "draggable",
              {
                key: _vm.id,
                attrs: {
                  move: _vm.checkMove,
                  options: { group: _vm.id, draggable: ".releases" }
                },
                model: {
                  value: _vm.releases,
                  callback: function($$v) {
                    _vm.releases = $$v
                  },
                  expression: "releases"
                }
              },
              [
                _c(
                  "transition-group",
                  { attrs: { tag: "ul" } },
                  _vm._l(_vm.releases, function(release, indextr) {
                    return _c(
                      "li",
                      { key: indextr + 1, staticClass: "releases" },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "vx-row flex justify-between cursor-pointer p-3",
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                return _vm.editData(release)
                              }
                            }
                          },
                          [
                            _c(
                              "div",
                              {
                                staticClass:
                                  "vx-col flex flex-row vs-lg-5 vs-sm-3 vs-xs-3 ml-0 pl-0 items-center"
                              },
                              [
                                _c("vs-icon", {
                                  staticClass: "cursor-move",
                                  attrs: { icon: "drag_indicator" }
                                }),
                                _vm._v(" "),
                                _c(
                                  "vx-tooltip",
                                  { attrs: { text: release.name } },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm._f("truncate")(release.name, 25)
                                      )
                                    )
                                  ]
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "vx-col vs-lg-3 vs-sm-4 vs-xs-4 text-right"
                              },
                              [
                                _vm._v(
                                  _vm._s(_vm._f("money_label")(release.planned))
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "vx-col vs-lg-3 vs-sm-4 vs-xs-4 text-right"
                              },
                              [
                                _vm._v(
                                  _vm._s(
                                    _vm._f("money_label")(
                                      _vm.totalReceived(release.receiveds)
                                    )
                                  )
                                )
                              ]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("vs-divider", { staticClass: "md:w-full my-1" })
                      ],
                      1
                    )
                  }),
                  0
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "vx-row flex justify-between p-3" }, [
              _c("div", {
                staticClass:
                  "vx-col flex flex-row vs-lg-5 vs-sm-3 vs-xs-3 ml-0 pl-0 items-center"
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "vx-row" }, [
              _c(
                "div",
                {
                  staticClass:
                    "btn-add-new p-3 my-3 mx-4 rounded-lg cursor-pointer flex items-center justify-center text-lg font-medium text-base text-primary border border-solid border-primary",
                  on: { click: _vm.addNewData }
                },
                [
                  _c("feather-icon", {
                    attrs: { icon: "PlusIcon", svgClasses: "h-4 w-4" }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "ml-2 text-base text-primary" }, [
                    _vm._v("Add Previsão")
                  ])
                ],
                1
              )
            ])
          ]
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=template&id=33af71ee&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=template&id=33af71ee& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("transition", { attrs: { name: "fade" } }, [
        !_vm.activeAddReceived
          ? _c(
              "div",
              { staticClass: "vx-row w-full flex justify-between m-0 mb-2" },
              [
                _c("div", { staticClass: "vx-col pt-2 p-0" }, [
                  _vm._v(_vm._s(_vm.totalDataReceivedsLocal) + " Recebido(s)")
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "vs-col pt-1 p-0" },
                  [
                    _c(
                      "vs-button",
                      {
                        attrs: {
                          size: "small",
                          color: "primary",
                          type: "border",
                          "icon-pack": "feather",
                          icon: "icon-plus"
                        },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.toggleAddList(true)
                          }
                        }
                      },
                      [_vm._v("Add")]
                    )
                  ],
                  1
                )
              ]
            )
          : _vm._e()
      ]),
      _vm._v(" "),
      !_vm.activeAddReceived
        ? [
            _c(
              "draggable",
              {
                model: {
                  value: _vm.receivedsListLocal,
                  callback: function($$v) {
                    _vm.receivedsListLocal = $$v
                  },
                  expression: "receivedsListLocal"
                }
              },
              [
                _c(
                  "transition-group",
                  { attrs: { tag: "ul" } },
                  _vm._l(_vm.receivedsListLocal, function(item, index) {
                    return _c(
                      "li",
                      {
                        key: index + 1,
                        staticClass: "cursor-pointer todo_todo-item",
                        style: [{ transitionDelay: index * 0.1 + "s" }]
                      },
                      [
                        _c("div", { staticClass: "px-1 py-1" }, [
                          _c("div", { staticClass: "vx-row" }, [
                            _c(
                              "div",
                              { staticClass: "vx-col w-4/6 flex flex-row" },
                              [
                                _c(
                                  "div",
                                  { staticClass: "flex text-primary" },
                                  [_vm._v(_vm._s(_vm._f("date")(item.date)))]
                                ),
                                _vm._v(" "),
                                _c("div", { staticClass: "flex ml-2" }, [
                                  _vm._v(
                                    _vm._s(_vm._f("truncate")(item.name, 15))
                                  )
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "vx-col w-2/6 ml-auto flex justify-end text-success"
                              },
                              [
                                _vm._v(
                                  "\n                " +
                                    _vm._s(
                                      _vm._f("money_label")(item.received)
                                    ) +
                                    "\n                "
                                ),
                                _c("feather-icon", {
                                  staticClass:
                                    "cursor-pointer list-item-component ml-1",
                                  attrs: {
                                    icon: "TrashIcon",
                                    svgClasses: "text-danger w-5 h-5"
                                  },
                                  on: {
                                    click: function($event) {
                                      $event.stopPropagation()
                                      return _vm.moveToReceivedLocal(index + 1)
                                    }
                                  }
                                })
                              ],
                              1
                            )
                          ])
                        ])
                      ]
                    )
                  }),
                  0
                )
              ],
              1
            )
          ]
        : _vm._e(),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        _vm.activeAddReceived
          ? _c("div", { staticClass: "vx-row flex" }, [
              _c(
                "div",
                { staticClass: "vx-col vs-lg-5 vs-sm-5 vs-xs-5 pr-1 mt-2" },
                [
                  _c("datepicker", {
                    ref: "programaticOpen",
                    attrs: {
                      format: _vm.format,
                      language: _vm.languages[_vm.language],
                      "input-class":
                        "vs-inputx vs-input--input normal hasValue w-full w-6"
                    },
                    model: {
                      value: _vm.receivedLocal.date,
                      callback: function($$v) {
                        _vm.$set(_vm.receivedLocal, "date", $$v)
                      },
                      expression: "receivedLocal.date"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "vx-col vs-lg-7 vs-sm-7 vs-xs-7 pl-1 mt-2" },
                [
                  _c("vs-input", {
                    staticClass: "w-full",
                    attrs: { placeholder: "Descrição" },
                    model: {
                      value: _vm.receivedLocal.name,
                      callback: function($$v) {
                        _vm.$set(_vm.receivedLocal, "name", $$v)
                      },
                      expression: "receivedLocal.name"
                    }
                  })
                ],
                1
              )
            ])
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        _vm.activeAddReceived
          ? _c("div", { staticClass: "vx-row flex" }, [
              _c(
                "div",
                { staticClass: "vx-col vs-lg-12 vs-sm-12 vs-xs-12" },
                [
                  _c(
                    "vx-input-group",
                    { staticClass: "mt-2 w-full" },
                    [
                      _c("template", { slot: "prepend" }, [
                        _c("div", { staticClass: "prepend-text bg-primary" }, [
                          _c("span", [_vm._v("R$")])
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "money",
                        _vm._b(
                          {
                            staticClass:
                              "vs-inputx vs-input--input normal hasValue",
                            model: {
                              value: _vm.receivedLocal.received,
                              callback: function($$v) {
                                _vm.$set(_vm.receivedLocal, "received", $$v)
                              },
                              expression: "receivedLocal.received"
                            }
                          },
                          "money",
                          _vm.money,
                          false
                        )
                      )
                    ],
                    2
                  )
                ],
                1
              )
            ])
          : _vm._e()
      ]),
      _vm._v(" "),
      !_vm.activeAddReceived
        ? _c("div", { staticClass: "vx-row flex" }, [_c("vs-divider")], 1)
        : _vm._e(),
      _vm._v(" "),
      !_vm.activeAddReceived
        ? _c(
            "div",
            { staticClass: "flex justify-between font-semibold mb-3" },
            [
              _c("div", [_vm._v("Total de Recebidos")]),
              _vm._v(" "),
              _c("div", [
                _vm._v(
                  _vm._s(_vm._f("money_label")(_vm.totalValueReceivedsLocal))
                )
              ])
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        _vm.activeAddReceived
          ? _c("div", { staticClass: "vx-row flex justify-between" }, [
              _c(
                "div",
                { staticClass: "vx-col mt-2" },
                [
                  _c(
                    "vs-button",
                    {
                      attrs: {
                        disabled: !_vm.validateForm,
                        size: "small",
                        color: "warning",
                        type: "filled",
                        "icon-pack": "feather",
                        icon: "icon-plus"
                      },
                      on: {
                        click: function($event) {
                          $event.stopPropagation()
                          return _vm.addReceivedLocal($event)
                        }
                      }
                    },
                    [_vm._v("Incluir")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "vx-col mt-2" },
                [
                  _c(
                    "vs-button",
                    {
                      attrs: { size: "small", color: "danger", type: "border" },
                      on: {
                        click: function($event) {
                          $event.stopPropagation()
                          return _vm.toggleAddList(false)
                        }
                      }
                    },
                    [_vm._v("Cancelar")]
                  )
                ],
                1
              )
            ])
          : _vm._e()
      ])
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=template&id=54b703a0&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=template&id=54b703a0&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "vs-sidebar",
    {
      staticClass: "add-new-data-sidebar items-no-padding",
      attrs: {
        "click-not-close": "",
        "position-right": "",
        parent: "body",
        "default-index": "1",
        color: "primary",
        spacer: ""
      },
      model: {
        value: _vm.isSidebarActiveLocal,
        callback: function($$v) {
          _vm.isSidebarActiveLocal = $$v
        },
        expression: "isSidebarActiveLocal"
      }
    },
    [
      _c(
        "div",
        { staticClass: "mt-6 flex items-center justify-between px-6" },
        [
          _c("h4", [
            _vm._v(
              _vm._s(
                Object.entries(this.data).length === 0 ? "ADICIONAR" : "ALTERAR"
              ) + " PREVISÃO"
            )
          ]),
          _vm._v(" "),
          _c("feather-icon", {
            staticClass: "cursor-pointer",
            attrs: { icon: "XIcon" },
            on: {
              click: function($event) {
                $event.stopPropagation()
                _vm.isSidebarActiveLocal = false
              }
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("vs-divider", { staticClass: "mb-0" }),
      _vm._v(" "),
      _c(
        "VuePerfectScrollbar",
        {
          key: _vm.$vs.rtl,
          staticClass: "scroll-area--data-list-add-new",
          attrs: { settings: _vm.settings }
        },
        [
          _c(
            "div",
            { staticClass: "p-6 pt-2" },
            [
              _c("vs-input", {
                directives: [
                  {
                    name: "validate",
                    rawName: "v-validate",
                    value: "required",
                    expression: "'required'"
                  }
                ],
                staticClass: "mt-5 w-full",
                attrs: { label: "Descrição", name: "item-name" },
                model: {
                  value: _vm.dataName,
                  callback: function($$v) {
                    _vm.dataName = $$v
                  },
                  expression: "dataName"
                }
              }),
              _vm._v(" "),
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.errors.has("item-name"),
                      expression: "errors.has('item-name')"
                    }
                  ],
                  staticClass: "text-danger text-sm"
                },
                [_vm._v(_vm._s(_vm.errors.first("item-name")))]
              ),
              _vm._v(" "),
              _c(
                "vs-select",
                {
                  directives: [
                    {
                      name: "validate",
                      rawName: "v-validate",
                      value: "required",
                      expression: "'required'"
                    }
                  ],
                  staticClass: "mt-5 w-full",
                  attrs: {
                    autocomplete: "",
                    label: "Tipo Previsão",
                    name: "item-category"
                  },
                  model: {
                    value: _vm.dataCategory,
                    callback: function($$v) {
                      _vm.dataCategory = $$v
                    },
                    expression: "dataCategory"
                  }
                },
                _vm._l(_vm.category_previsions, function(item) {
                  return _c("vs-select-item", {
                    key: item.value,
                    attrs: { value: item.value, text: item.text }
                  })
                }),
                1
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.errors.has("item-category"),
                      expression: "errors.has('item-category')"
                    }
                  ],
                  staticClass: "text-danger text-sm"
                },
                [_vm._v(_vm._s(_vm.errors.first("item-category")))]
              ),
              _vm._v(" "),
              _c(
                "vx-input-group",
                { staticClass: "mb-base mt-5 w-full" },
                [
                  _c("template", { slot: "prepend" }, [
                    _c("div", { staticClass: "prepend-text bg-primary" }, [
                      _c("span", [_vm._v("Planejado")])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("template", { slot: "prepend" }, [
                    _c("div", { staticClass: "prepend-text bg-primary" }, [
                      _c("span", [_vm._v("R$")])
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "money",
                    _vm._b(
                      {
                        directives: [
                          {
                            name: "validate",
                            rawName: "v-validate",
                            value: { required: true },
                            expression: "{ required: true }"
                          }
                        ],
                        staticClass:
                          "vs-inputx vs-input--input normal hasValue",
                        attrs: { name: "Valor Planejado" },
                        model: {
                          value: _vm.dataPlanned,
                          callback: function($$v) {
                            _vm.dataPlanned = $$v
                          },
                          expression: "dataPlanned"
                        }
                      },
                      "money",
                      _vm.money,
                      false
                    )
                  )
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.errors.has("Valor Planejado"),
                      expression: "errors.has('Valor Planejado')"
                    }
                  ],
                  staticClass: "text-danger text-sm"
                },
                [_vm._v(_vm._s(_vm.errors.first("Valor Planejado")))]
              ),
              _vm._v(" "),
              _c(
                "vs-divider",
                { staticClass: "mb-1", attrs: { color: "primary" } },
                [_vm._v("Recebidos")]
              ),
              _vm._v(" "),
              _c("received")
            ],
            1
          )
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "flex flex-wrap items-center p-6",
          attrs: { slot: "footer" },
          slot: "footer"
        },
        [
          _c(
            "vs-button",
            {
              staticClass: "mr-6",
              attrs: { disabled: !_vm.isFormValid },
              on: { click: _vm.submitData }
            },
            [_vm._v("Salvar")]
          ),
          _vm._v(" "),
          _c(
            "vs-button",
            {
              attrs: { type: "border", color: "danger" },
              on: {
                click: function($event) {
                  _vm.isSidebarActiveLocal = false
                }
              }
            },
            [_vm._v("Cancelar")]
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/store/prevision/modulePrevision.js":
/*!*************************************************************!*\
  !*** ./resources/js/src/store/prevision/modulePrevision.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modulePrevisionState_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modulePrevisionState.js */ "./resources/js/src/store/prevision/modulePrevisionState.js");
/* harmony import */ var _modulePrevisionMutations_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modulePrevisionMutations.js */ "./resources/js/src/store/prevision/modulePrevisionMutations.js");
/* harmony import */ var _modulePrevisionActions_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modulePrevisionActions.js */ "./resources/js/src/store/prevision/modulePrevisionActions.js");
/* harmony import */ var _modulePrevisionGetters_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modulePrevisionGetters.js */ "./resources/js/src/store/prevision/modulePrevisionGetters.js");
/*=========================================================================================
  File Name: modulePrevision.js
  Description: Prevision Module
  ----------------------------------------------------------------------------------------
  Item Name: Finx
  Author: André M Fagundes
  Author URL: https://github.com/andrefagundes
==========================================================================================*/




/* harmony default export */ __webpack_exports__["default"] = ({
  isRegistered: false,
  namespaced: true,
  state: _modulePrevisionState_js__WEBPACK_IMPORTED_MODULE_0__["default"],
  mutations: _modulePrevisionMutations_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  actions: _modulePrevisionActions_js__WEBPACK_IMPORTED_MODULE_2__["default"],
  getters: _modulePrevisionGetters_js__WEBPACK_IMPORTED_MODULE_3__["default"]
});

/***/ }),

/***/ "./resources/js/src/store/prevision/modulePrevisionActions.js":
/*!********************************************************************!*\
  !*** ./resources/js/src/store/prevision/modulePrevisionActions.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/axios.js */ "./resources/js/src/axios.js");
/*=========================================================================================
  File Name: modulePrevisionActions.js
  Description: Prevision Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Finx
  Author: André M Fagundes
  Author URL: https://github.com/andrefagundes
==========================================================================================*/

/* harmony default export */ __webpack_exports__["default"] = ({
  addItem: function addItem(_ref, item) {
    var commit = _ref.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/prevision/releases/", {
        item: item
      }).then(function (response) {
        commit('ADD_ITEM', Object.assign(item, {
          id: response.data.id
        }));
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchPrevisions: function fetchPrevisions(_ref2, payload) {
    var commit = _ref2.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/api/previsions/").then(function (response) {
        commit('SET_PREVISIONS', response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateItem: function updateItem(_ref3, item) {
    var commit = _ref3.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/prevision/releases/".concat(item.id), {
        item: item
      }).then(function (response) {
        commit('UPDATE_RELEASES', response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeItem: function removeItem(_ref4, itemId) {
    var commit = _ref4.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].delete("/api/prevision/releases/".concat(itemId)).then(function (response) {
        commit('REMOVE_ITEM', itemId);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  addReceivedLocal: function addReceivedLocal(_ref5, received) {
    var commit = _ref5.commit;
    commit('ADD_RECEIVED_LOCAL', Object.assign({}, received));
  },
  removeReceivedLocal: function removeReceivedLocal(_ref6, itemId) {
    var commit = _ref6.commit;
    commit('REMOVE_RECEIVED_LOCAL', itemId);
  }
});

/***/ }),

/***/ "./resources/js/src/store/prevision/modulePrevisionGetters.js":
/*!********************************************************************!*\
  !*** ./resources/js/src/store/prevision/modulePrevisionGetters.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: modulePrevisionGetters.js
  Description: Prevision Module Getters
  ----------------------------------------------------------------------------------------
  Item Name: Finx
  Author: André M Fagundes
  Author URL: https://github.com/andrefagundes
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  getTotalReceiveds: function getTotalReceiveds(state) {
    return function (itemId) {
      var index = state.previsions.findIndex(function (item) {
        return item.id == itemId;
      });
      return state.previsions[index].releases.map(function (prod) {
        return prod.receiveds.reduce(function (total, valor) {
          return total + valor.received;
        }, 0);
      }).reduce(function (total, valor) {
        return total + valor;
      }, 0);
    };
  },
  getTotalPlanned: function getTotalPlanned(state) {
    return function (itemId) {
      var index = state.previsions.findIndex(function (item) {
        return item.id == itemId;
      });
      return state.previsions[index].releases.reduce(function (total, valor) {
        return total + valor.planned;
      }, 0);
    };
  },
  getTotalValueReceivedsLocal: function getTotalValueReceivedsLocal(state) {
    return state.receiveds_local.reduce(function (total, valor) {
      return total + valor.received;
    }, 0);
  },
  getTotalDataReceivedsLocal: function getTotalDataReceivedsLocal(state) {
    return state.receiveds_local.length;
  }
});

/***/ }),

/***/ "./resources/js/src/store/prevision/modulePrevisionMutations.js":
/*!**********************************************************************!*\
  !*** ./resources/js/src/store/prevision/modulePrevisionMutations.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: modulePrevisionMutations.js
  Description: Prevision Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Finx
  Author: André M Fagundes
  Author URL: https://github.com/andrefagundes
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  SET_PREVISIONS: function SET_PREVISIONS(state, previsions) {
    state.previsions = previsions;
  },
  ADD_ITEM: function ADD_ITEM(state, item) {
    state.releases.unshift(item);
  },
  SET_RELEASES: function SET_RELEASES(state, releases) {
    state.releases = releases;
  },
  UPDATE_RELEASES: function UPDATE_RELEASES(state, release) {
    var releaseIndex = state.releases.findIndex(function (p) {
      return p.id == release.id;
    });
    Object.assign(state.releases[releaseIndex], release);
  },
  REMOVE_ITEM: function REMOVE_ITEM(state, itemId) {
    var ItemIndex = state.releases.findIndex(function (p) {
      return p.id == itemId;
    });
    state.releases.splice(ItemIndex, 1);
  },
  //RECEIVEDS LOCAL
  SET_RECEIVEDS_LOCAL: function SET_RECEIVEDS_LOCAL(state, receiveds) {
    state.receiveds_local = receiveds;
  },
  ADD_RECEIVED_LOCAL: function ADD_RECEIVED_LOCAL(state, receivedLocal) {
    state.receiveds_local.unshift(receivedLocal);
  },
  REMOVE_RECEIVED_LOCAL: function REMOVE_RECEIVED_LOCAL(state, itemId) {
    var ItemIndex = state.receiveds_local.findIndex(function (p) {
      return p.id == itemId;
    });
    state.receiveds_local.splice(ItemIndex, 1);
  },
  INIT_RECEIVEDS_LOCAL: function INIT_RECEIVEDS_LOCAL(state) {
    state.receiveds_local = [];
  }
});

/***/ }),

/***/ "./resources/js/src/store/prevision/modulePrevisionState.js":
/*!******************************************************************!*\
  !*** ./resources/js/src/store/prevision/modulePrevisionState.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: modulePrevisonState.js
  Description: Prevision Module State
  ----------------------------------------------------------------------------------------
  Item Name: Finx
  Author: André M Fagundes
  Author URL: https://github.com/andrefagundes
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  previsions: [],
  receiveds_local: []
});

/***/ }),

/***/ "./resources/js/src/views/prevision/Prevision.vue":
/*!********************************************************!*\
  !*** ./resources/js/src/views/prevision/Prevision.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _Prevision_vue_vue_type_template_id_3be9c2cd___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Prevision.vue?vue&type=template&id=3be9c2cd& */ "./resources/js/src/views/prevision/Prevision.vue?vue&type=template&id=3be9c2cd&");
/* harmony import */ var _Prevision_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Prevision.vue?vue&type=script&lang=js& */ "./resources/js/src/views/prevision/Prevision.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Prevision_vue_vue_type_style_index_0_module_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Prevision.vue?vue&type=style&index=0&module=true&lang=css& */ "./resources/js/src/views/prevision/Prevision.vue?vue&type=style&index=0&module=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





var cssModules = {}
var disposed = false

function injectStyles (context) {
  if (disposed) return
  
        cssModules["$style"] = (_Prevision_vue_vue_type_style_index_0_module_true_lang_css___WEBPACK_IMPORTED_MODULE_2__["default"].locals || _Prevision_vue_vue_type_style_index_0_module_true_lang_css___WEBPACK_IMPORTED_MODULE_2__["default"])
        Object.defineProperty(this, "$style", {
          configurable: true,
          get: function () {
            return cssModules["$style"]
          }
        })
      
}


  module.hot && false



        module.hot && false

/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Prevision_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Prevision_vue_vue_type_template_id_3be9c2cd___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Prevision_vue_vue_type_template_id_3be9c2cd___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  injectStyles,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/prevision/Prevision.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./resources/js/src/views/prevision/Prevision.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/src/views/prevision/Prevision.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Prevision_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Prevision.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/Prevision.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Prevision_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/prevision/Prevision.vue?vue&type=style&index=0&module=true&lang=css&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/src/views/prevision/Prevision.vue?vue&type=style&index=0&module=true&lang=css& ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Prevision_vue_vue_type_style_index_0_module_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--7-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Prevision.vue?vue&type=style&index=0&module=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/Prevision.vue?vue&type=style&index=0&module=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Prevision_vue_vue_type_style_index_0_module_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Prevision_vue_vue_type_style_index_0_module_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Prevision_vue_vue_type_style_index_0_module_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Prevision_vue_vue_type_style_index_0_module_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Prevision_vue_vue_type_style_index_0_module_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/prevision/Prevision.vue?vue&type=template&id=3be9c2cd&":
/*!***************************************************************************************!*\
  !*** ./resources/js/src/views/prevision/Prevision.vue?vue&type=template&id=3be9c2cd& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Prevision_vue_vue_type_template_id_3be9c2cd___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Prevision.vue?vue&type=template&id=3be9c2cd& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/Prevision.vue?vue&type=template&id=3be9c2cd&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Prevision_vue_vue_type_template_id_3be9c2cd___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Prevision_vue_vue_type_template_id_3be9c2cd___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/prevision/PrevisionAddNewGroup.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/src/views/prevision/PrevisionAddNewGroup.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PrevisionAddNewGroup_vue_vue_type_template_id_432738ba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PrevisionAddNewGroup.vue?vue&type=template&id=432738ba& */ "./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=template&id=432738ba&");
/* harmony import */ var _PrevisionAddNewGroup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PrevisionAddNewGroup.vue?vue&type=script&lang=js& */ "./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PrevisionAddNewGroup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PrevisionAddNewGroup_vue_vue_type_template_id_432738ba___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PrevisionAddNewGroup_vue_vue_type_template_id_432738ba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/prevision/PrevisionAddNewGroup.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PrevisionAddNewGroup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PrevisionAddNewGroup.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PrevisionAddNewGroup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=template&id=432738ba&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=template&id=432738ba& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrevisionAddNewGroup_vue_vue_type_template_id_432738ba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PrevisionAddNewGroup.vue?vue&type=template&id=432738ba& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/PrevisionAddNewGroup.vue?vue&type=template&id=432738ba&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrevisionAddNewGroup_vue_vue_type_template_id_432738ba___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrevisionAddNewGroup_vue_vue_type_template_id_432738ba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/prevision/PrevisionCard.vue":
/*!************************************************************!*\
  !*** ./resources/js/src/views/prevision/PrevisionCard.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PrevisionCard_vue_vue_type_template_id_0201777d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PrevisionCard.vue?vue&type=template&id=0201777d& */ "./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=template&id=0201777d&");
/* harmony import */ var _PrevisionCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PrevisionCard.vue?vue&type=script&lang=js& */ "./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PrevisionCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PrevisionCard_vue_vue_type_template_id_0201777d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PrevisionCard_vue_vue_type_template_id_0201777d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/prevision/PrevisionCard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PrevisionCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PrevisionCard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PrevisionCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=template&id=0201777d&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=template&id=0201777d& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrevisionCard_vue_vue_type_template_id_0201777d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PrevisionCard.vue?vue&type=template&id=0201777d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/PrevisionCard.vue?vue&type=template&id=0201777d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrevisionCard_vue_vue_type_template_id_0201777d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrevisionCard_vue_vue_type_template_id_0201777d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/prevision/ReceivedAddNew.vue":
/*!*************************************************************!*\
  !*** ./resources/js/src/views/prevision/ReceivedAddNew.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ReceivedAddNew_vue_vue_type_template_id_33af71ee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ReceivedAddNew.vue?vue&type=template&id=33af71ee& */ "./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=template&id=33af71ee&");
/* harmony import */ var _ReceivedAddNew_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ReceivedAddNew.vue?vue&type=script&lang=js& */ "./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ReceivedAddNew_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ReceivedAddNew.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ReceivedAddNew_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ReceivedAddNew_vue_vue_type_template_id_33af71ee___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ReceivedAddNew_vue_vue_type_template_id_33af71ee___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/prevision/ReceivedAddNew.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ReceivedAddNew_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ReceivedAddNew.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ReceivedAddNew_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ReceivedAddNew_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ReceivedAddNew.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ReceivedAddNew_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ReceivedAddNew_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ReceivedAddNew_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ReceivedAddNew_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ReceivedAddNew_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=template&id=33af71ee&":
/*!********************************************************************************************!*\
  !*** ./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=template&id=33af71ee& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ReceivedAddNew_vue_vue_type_template_id_33af71ee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ReceivedAddNew.vue?vue&type=template&id=33af71ee& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/prevision/ReceivedAddNew.vue?vue&type=template&id=33af71ee&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ReceivedAddNew_vue_vue_type_template_id_33af71ee___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ReceivedAddNew_vue_vue_type_template_id_33af71ee___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue":
/*!**************************************************************************!*\
  !*** ./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DataViewSidebar_vue_vue_type_template_id_54b703a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DataViewSidebar.vue?vue&type=template&id=54b703a0&scoped=true& */ "./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=template&id=54b703a0&scoped=true&");
/* harmony import */ var _DataViewSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DataViewSidebar.vue?vue&type=script&lang=js& */ "./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _DataViewSidebar_vue_vue_type_style_index_0_id_54b703a0_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true& */ "./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _DataViewSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DataViewSidebar_vue_vue_type_template_id_54b703a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DataViewSidebar_vue_vue_type_template_id_54b703a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "54b703a0",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DataViewSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./DataViewSidebar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DataViewSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true&":
/*!************************************************************************************************************************************!*\
  !*** ./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_DataViewSidebar_vue_vue_type_style_index_0_id_54b703a0_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=style&index=0&id=54b703a0&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_DataViewSidebar_vue_vue_type_style_index_0_id_54b703a0_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_DataViewSidebar_vue_vue_type_style_index_0_id_54b703a0_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_DataViewSidebar_vue_vue_type_style_index_0_id_54b703a0_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_DataViewSidebar_vue_vue_type_style_index_0_id_54b703a0_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_DataViewSidebar_vue_vue_type_style_index_0_id_54b703a0_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=template&id=54b703a0&scoped=true&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=template&id=54b703a0&scoped=true& ***!
  \*********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DataViewSidebar_vue_vue_type_template_id_54b703a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./DataViewSidebar.vue?vue&type=template&id=54b703a0&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/ui-elements/prevision/DataViewSidebar.vue?vue&type=template&id=54b703a0&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DataViewSidebar_vue_vue_type_template_id_54b703a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DataViewSidebar_vue_vue_type_template_id_54b703a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);